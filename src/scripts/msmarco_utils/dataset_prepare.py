from utils import *
import asyncio
import aiohttp

class DatasetPreparer:
    def __init__(self, name_dataset="msmarco-document/trec-dl-2019"):
        self.name_dataset = name_dataset
        self.dataset = ir_datasets.load(self.name_dataset)
        self.thread_cnt = 6
        self.batch_size = 250

    def get_relevance_for_query(self, out_filename='tmp.txt'):
        docs = dict()
        for doc in self.dataset.docs_iter():
            docs[doc.doc_id] = doc.url

        qrels = dict()
        for qrel in self.dataset.qrels_iter():
            if qrel.query_id not in qrels:
                qrels[qrel.query_id] = []
            qrels[qrel.query_id].append({"doc_id": docs[qrel.doc_id], "relevance": int(qrel.relevance)})

        for key in qrels.keys():
            qrels[key] = sorted(qrels[key], key=lambda x: x['relevance'], reverse=True)
            qrels[key] = [v['doc_id'] for v in qrels[key] if v['relevance'] > 0]

        qrels_ans = []
        for query in self.dataset.queries_iter():
            if query.query_id in qrels:
                qrels_ans.append(json.dumps({"query": query.text, "urls": qrels[query.query_id]}))

        with open(out_filename, 'a+') as of:
            json.dump(qrels_ans, of)

    def save_dataset(self, out_filename="index_dataset.txt"):
        docs_ans = []
        for doc in self.dataset.docs_iter():
            docs_ans = {"url": doc.url, "title": doc.title, "body": doc.body}

        with open(out_filename, 'w') as out_f:
            json.dump(docs_ans, out_f)

    def get_text_selectolax(self, html):
        tree = HTMLParser(html)

        if tree.body is None:
            return None

        for tag in tree.css('script'):
            tag.decompose()
        for tag in tree.css('style'):
            tag.decompose()

        tree.merge_text_nodes()
        text = tree.body.text(deep=True, separator='\n', strip=True)
        text = re.sub(r"\n+", "\n", text)
        title_node = tree.css_first('title')
        title = ""
        if title_node:
            title = title_node.text()
        return title, text

    def get_text_from_url(self, url: str):
        try:
            with requests.get(url, timeout=0.6) as page:
                if page.status_code >= 400:
                    return False, None

                html = page.text.strip('\n')
                # soup = BeautifulSoup(html, features="html.parser")
                # body_text = '\n'.join(soup.stripped_strings)
                # data = {'title': soup.find('title').text, 'body': body_text.strip()}
                title, body = self.get_text_selectolax(html)
                data = {'title': title, "body": body}
                return (True, data)
        except Exception as e:
            return (False, None)

    def get_text_with_render(self, driver, url):
        try:
            driver.get(url)
            data = {"title": driver.title, "body": driver.find_element(By.XPATH, "/html/body").text}
            return True, data
        except Exception as e:
            print(url)
            return False, None


    def update_dataset(self):
        html_dataset = load_json(f"new_dataset/{self.name_dataset}_html.txt")
        part_of_initial_dataset = load_json(f"new_dataset/{self.name_dataset}_initial.txt")
        updated_dataset = []
        match_percentage = 0.10
        match_stat = {"replaced": 0, "total": 0}
        for i in range(len(html_dataset)):
            if html_dataset[i]["inaccessible"] or compare_two_text_with_percent(html_dataset[i]["body"], part_of_initial_dataset[i]["body"]) < match_percentage:
                updated_dataset.append(part_of_initial_dataset[i])
            else:
                updated_dataset.append(html_dataset[i])
                match_stat["replaced"] += 1
            match_stat["total"] += 1

        save_json(f"new_dataset/{self.name_dataset}_updated.txt", updated_dataset)
        save_json(f"new_dataset/{self.name_dataset}_updated_log.txt", match_stat)
    """
        Save all urls into *.txt doc from dataset
    """
    def save_urls_from_dataset(self, out_file="new_dataset/msmarco-document/urls.txt"):
        create_file(out_file)
        urls_list = []
        for i, doc in enumerate(self.dataset.docs_iter()):
            urls_list.append({"doc_id": doc.doc_id, "url": doc.url})

            if i % 10000 == 0:
                cur_time = datetime.datetime.now().time()
                print(f"documents processed: {i}, time: {cur_time}")

        save_json(out_file, urls_list)

    """
        Saving html text in the thread by urls sublist 
    """
    def save_html_text_from_suburls(self, id, urls_sublist, shift):
        new_dataset = []
        stat = {"inaccessible": 0, "total": 0}
        for i, doc in enumerate(urls_sublist):
            has_text, data = self.get_text_from_url(doc["url"])
            if has_text:
                new_dataset.append(
                    {"url": doc["url"], "title": data["title"], "body": data["body"], "doc_id": doc["doc_id"],
                     "inaccessible": False})
            else:
                new_dataset.append({"url": doc["url"], "title": "", "body": "", "doc_id": doc["doc_id"],
                                    "inaccessible": True})
                stat["inaccessible"] += 1

            stat["total"] += 1
            if i % self.batch_size == 0 and i != 0:
                out_file = f"new_dataset/{self.name_dataset}_html_{id}/{i + shift}.txt"
                log_file = f"new_dataset/{self.name_dataset}_html_{id}/log_{i + shift}.txt"
                create_file(out_file)
                save_json(out_file, new_dataset)
                save_json(log_file, stat)
                new_dataset.clear()

                cur_time = datetime.datetime.now().time()
                print(f"thead {id}: documents processed: {i}, time: {cur_time}")
            if i == 25000:
                break


    """
        Run dataset updater in n-thread
    """
    def create_new_dataset_from_urls(self, furls="new_dataset/msmarco-document/urls.txt", shift=5000):
        with open(furls, "r") as f:
            urls_list = json.load(f)

            total_urls = len(urls_list)
            data_per_thread = int(total_urls / self.thread_cnt)
            threads = []

            acc = 0
            for i in range(self.thread_cnt):
                print(f"start processing thread {i}: ")
                t = Thread(target=self.save_html_text_from_suburls, args=[i, urls_list[acc + shift: acc + data_per_thread], shift])
                t.start()
                threads.append(t)

                acc += data_per_thread

            for thread in threads:
                thread.join()

            print("THE END")


    def concat_sub_dataset(self):
        whole_dataset, total_stat = [], {"inaccessible": 0, "total": 0}
        for i in range(self.thread_cnt):
            cnt_urls = self.batch_size
            sub_dataset = []
            try:
                while(1) :
                    out_file = f"new_dataset/{self.name_dataset}_html_{i}/{cnt_urls}.txt"
                    sub_dataset += load_json(out_file)
                    cnt_urls += self.batch_size
            except Exception as e:
                save_json(f"new_dataset/{self.name_dataset}_html_{i}/total.txt", sub_dataset)
                whole_dataset += sub_dataset

                stat_file = f"new_dataset/{self.name_dataset}_html_{i}/log_{cnt_urls - self.batch_size}.txt"
                tmp_stat = load_json(stat_file)
                total_stat["inaccessible"] += tmp_stat["inaccessible"]
                total_stat["total"] += tmp_stat["total"]


        save_json(f"new_dataset/{self.name_dataset}_html.txt", whole_dataset)
        save_json(f"new_dataset/{self.name_dataset}_html_log.txt", total_stat)


    def save_part_dataset(self):
        html_dataset = load_json(f"new_dataset/{self.name_dataset}_html.txt")
        map_html_dataset = dict()
        for html_doc in html_dataset:
            map_html_dataset[html_doc["doc_id"]] = True
        part_dataset = []
        for doc in self.dataset.docs_iter():
            if doc.doc_id in map_html_dataset:
                part_dataset.append({"url": doc.url, "title": doc.title, "body": doc.body, "doc_id": doc.doc_id})

        save_json(f"new_dataset/{self.name_dataset}_initial.txt", part_dataset)