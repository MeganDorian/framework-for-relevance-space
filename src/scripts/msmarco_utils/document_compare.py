import argparse
import logging
import re
import zlib
import random
from datetime import datetime

import numpy as np

import ir_datasets
import mmh3
from nltk import ngrams

from utils import load_json, create_file, save_json, is_exist


class DocumentComparator:

    def __init__(self, name_dataset, batch_cnt, shingle_length):
        self.batch_cnt = batch_cnt if batch_cnt is not None else 800
        self.name_dataset = name_dataset if name_dataset is not None else "msmarco-document/trec-dl-2019"
        self.shingle_length = shingle_length if shingle_length is not None else 3
        self.percent_shingle_compare = 0.3

        self.cache_path = "new_dataset/cache_updated.txt"

        self.new_dataset_dir = f"new_dataset/{self.name_dataset}_html_async"
        self.updated_dataset_dir = f"new_dataset/{self.name_dataset}_updated"
    def log_info(self, text, fonly_debug=False):
        logging.info(text)
        if not fonly_debug:
            print(text)

    def hash_similarity(self, hash_a, hash_b):
        tmp_compare = int(len(hash_a) * self.percent_shingle_compare)
        if tmp_compare > hash_b.size:
            tmp_compare = int(len(hash_b) * self.percent_shingle_compare)
        top_a_shingles = hash_a[:tmp_compare]
        top_b_shingles = hash_b[:tmp_compare]
        return len(np.intersect1d(top_a_shingles, top_b_shingles)) / tmp_compare if tmp_compare != 0 else 0

    def compare_documents(self, old_doc, new_doc):
        if (15 * len(old_doc.body) < len(new_doc["body"])):
            return 0
        split_old_doc, split_new_doc = np.asarray(old_doc.body.split()), np.asarray(new_doc["body"].split())
        old_doc_shingles, new_doc_shingles = list(), list()
        for i in range(max(0,split_old_doc.size - self.shingle_length + 1)):
            value = mmh3.hash(' '.join(split_old_doc[i: i + self.shingle_length]))
            old_doc_shingles.append(value)

        for i in range(max(0, split_new_doc.size - self.shingle_length + 1)):
            value = mmh3.hash(' '.join(split_new_doc[i: i + self.shingle_length]))
            new_doc_shingles.append(value)
        old_doc_shingles, new_doc_shingles = np.array(old_doc_shingles), np.array(new_doc_shingles)
        old_doc_shingles = -np.sort(-old_doc_shingles)
        new_doc_shingles = -np.sort(-new_doc_shingles)
        return self.hash_similarity(old_doc_shingles, new_doc_shingles)

    def compare_batch(self, batch_id, new_dataset_batch, old_dataset):
        cache_info = load_json(self.cache_path)
        if batch_id in cache_info["saved_batches"]:
            for doc_i in range(len(new_dataset_batch)):
                old_doc = next(old_dataset)
            return

        stat = {"match": 0, "not match": 0, "inaccessible": 0}
        matches_stat = {}
        updated_batch = []
        for doc_i in range(len(new_dataset_batch)):
            old_doc, new_doc = next(old_dataset), new_dataset_batch[doc_i]

            doc = {"url": old_doc.url, "title": old_doc.title, "body": old_doc.body, "matches": False,
                   "inaccessible": False, "doc_id": old_doc.doc_id}

            if old_doc.doc_id == new_doc["doc_id"] and not new_doc["inaccessible"]:
                comparison_result = self.compare_documents(old_doc, new_doc)
                if comparison_result >= 0.05:
                    stat["match"] += 1
                    doc.update({"title": new_doc["title"], "body": new_doc["body"], "matches": True})
                else:
                    stat["not match"] += 1
                rounded_comparison_result = round(comparison_result, 4)
                if rounded_comparison_result in matches_stat:
                    matches_stat[rounded_comparison_result] += 1
                else:
                    matches_stat[rounded_comparison_result] = 1
            else:
                doc.update({"inaccessible": True})
                stat["inaccessible"] += 1

            updated_batch.append(doc)

        out_file = f"{self.updated_dataset_dir}/{batch_id}.txt"
        log_file = f"{self.updated_dataset_dir}/{batch_id}_log.txt"
        match_stat_file = f"{self.updated_dataset_dir}/{batch_id}_match_stat.txt"

        create_file(out_file)
        save_json(out_file, updated_batch)
        save_json(log_file, stat)

        matches_stat = dict(sorted(matches_stat.items()))
        save_json(match_stat_file, matches_stat)

        cache_info["saved_batches"].append(batch_id)
        save_json(self.cache_path, cache_info)

        self.log_info(stat)

    def compare_datasets(self):
        if not is_exist(self.cache_path) or load_json(self.cache_path)["num_batch"] != self.batch_cnt:
            self.create_cache_info()

        old_dataset = ir_datasets.load(self.name_dataset)
        old_dataset_docs, dataset_size = iter(old_dataset.docs_iter()), old_dataset.docs_count()
        for id in range(self.batch_cnt):
            new_dataset_docs = load_json(f"{self.new_dataset_dir}/{id}.txt")
            self.log_info(f"batch id: {id}, start time: {datetime.now().time()}")
            self.compare_batch(id, new_dataset_docs, old_dataset_docs)
            self.log_info(f"batch id: {id}, end time: {datetime.now().time()}")

    def create_cache_info(self):
        cache_info = {"num_batch": self.batch_cnt, "saved_batches": []}
        create_file(self.cache_path)
        save_json(self.cache_path, cache_info)

    def merge_dataset_parts(self):
        self.merge_updated_dataset_parts()
        self.merge_updated_dataset_log_parts()
        self.merge_match_stat()

    def merge_updated_dataset_parts(self):
        self.log_info("start of updated-dataset merging")
        with open(f"new_dataset/updated-msmarco-document.txt", "w") as out_file:
            out_file.write("[")
            for i in range(self.batch_cnt):
                try:
                    self.log_info(f"#{i} batch processed")
                    file_html_text = list(map(lambda x: str(x), load_json(f"{self.updated_dataset_dir}/{i}.txt")))
                    fullBatch = ', '.join(file_html_text)
                    out_file.write(fullBatch)
                    if i != self.batch_cnt - 1:
                        out_file.write(", ")
                except Exception as e:
                    pass
            out_file.write("]")
        self.log_info("end of updated-dataset merging")

    def merge_updated_dataset_log_parts(self):
        total_stat = {"match": 0, "not match": 0, "inaccessible": 0, "total" : 0}
        self.log_info("start of the stat dataset merging")
        for i in range(self.batch_cnt):
            try:
                sub_stat = load_json(f"{self.updated_dataset_dir}/{i}_log.txt")
                total_stat["match"] += sub_stat["match"]
                total_stat["not match"] += sub_stat["not match"]
                total_stat["inaccessible"] += sub_stat["inaccessible"]
                total_stat["total"] += sub_stat["inaccessible"] + sub_stat["match"] + sub_stat["not match"]
            except Exception as e:
                pass

        self.log_info("end of the stat updated dataset merging")
        save_json(f"new_dataset/updated-msmarco-document_log.txt", total_stat)
    def merge_match_stat(self):
        total_stat = {}
        self.log_info("start of the match stat dataset merging")
        for i in range(self.batch_cnt):
            try:
                match_stat = load_json(f"{self.updated_dataset_dir}/{i}_match_stat.txt")
                total_stat = {k: total_stat.get(k, 0) + match_stat.get(k, 0) for k in set(total_stat) | set(match_stat)}
            except Exception as e:
                pass

        self.log_info("end of the match stat updated dataset merging")
        save_json(f"new_dataset/updated-msmarco-document_match_stat.txt", total_stat)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, help='filepath to the dataset')
    parser.add_argument('--batch', type=int, help='count of batch')
    parser.add_argument('--shingles', type=int, help='count of batch')
    parser.add_argument('--run', action='store_true', help="start html-pages download")
    parser.add_argument('--merge_batches', action='store_true', help='merge part of the html dataset')
    parser.add_argument('--merge_log', action='store_true', help='merge part of the log dataset')
    parser.add_argument('--merge_stat', action='store_true', help='merge part of the match stat dataset')
    parser.add_argument('--merge', action='store_true', help="merge all parts of the dataset")

    args = parser.parse_args()
    dc = DocumentComparator(args.dataset, args.batch, args.shingles)

    if args.run:
        dc.compare_datasets()

    if args.merge:
        dc.merge_dataset_parts()
    else:
        if args.merge_batches:
            dc.merge_updated_dataset_parts()
        if args.merge_log:
            dc.merge_updated_dataset_log_parts()
        if args.merge_stat:
            dc.merge_match_stat()
