import json
from pathlib import Path


def load_file(filename: str) -> list:
    with open(filename) as file:
        return file.read().splitlines()


def load_json(filename: str):
    with open(filename, 'r') as f:
        return json.load(f)


def save_json(filename: str, data):
    with open(filename, 'w') as f:
        json.dump(data, f)


def encode_url(url):
    return url.replace("/", "$").replace(":", "#")


def decode_filename(filename):
    return filename.replace('#', ':').replace('$', '_')


def is_exist(filename):
    return Path(filename).is_file()

def create_file(new_path):
    output_file = Path(new_path)
    output_file.parent.mkdir(parents=True, exist_ok=True)
    output_file.write_text("")
