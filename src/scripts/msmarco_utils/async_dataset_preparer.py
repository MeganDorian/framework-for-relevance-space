import argparse
import json
import sys
from pathlib import Path
import ir_datasets
from utils import *
from selectolax.parser import HTMLParser
import datetime
import re
import logging
import asyncio
import aiohttp
import platform


class AsyncDatasetPreparer:
    def __init__(self, name_dataset, urls_path, batch_size):
        self.name_dataset = name_dataset if name_dataset is not None else "msmarco-document/trec-dl-2019"
        self.batch_size = batch_size if batch_size is not None else 800
        self.urls_path = urls_path if urls_path is not None else "new_dataset/msmarco-document/urls.txt"

        self.cache_path = "new_dataset/cache.txt"

        self.html_stat = {"inaccessible": 0, "total": 0}
        self.file_out_dir = f"new_dataset/{self.name_dataset}_html_async"

        logging.basicConfig(filename="log_info.txt", encoding='utf-8', level=logging.DEBUG)

    def log_info(self, text, fonly_debug=False):
        logging.info(text)
        if not fonly_debug:
            print(text)

    def save_urls_from_dataset(self, out_file="new_dataset/msmarco-document/urls.txt"):
        create_file(out_file)
        urls_list = []
        self.dataset = ir_datasets.load(self.name_dataset)
        for i, doc in enumerate(self.dataset.docs_iter()):
            urls_list.append({"doc_id": doc.doc_id, "url": doc.url})

            if i % 10000 == 0:
                cur_time = datetime.datetime.now().time()
                self.log_info(f"documents processed: {i}, time: {cur_time}")

        save_json(out_file, urls_list)

    def get_text_selectolax(self, html):
        tree = HTMLParser(html)

        if tree.body is None:
            return None

        for tag in tree.css('script'):
            tag.decompose()
        for tag in tree.css('style'):
            tag.decompose()

        tree.merge_text_nodes()
        text = tree.body.text(deep=True, separator='\n', strip=True)
        text = re.sub(r"\n+", "\n", text)
        title_node = tree.css_first('title')
        title = ""
        if title_node:
            title = title_node.text()
        return title, text

    async def get_text_from_url(self, url, session):
        result = {"url": url["url"], "title": "", "body": "", "doc_id": url["doc_id"], "inaccessible": True}
        try:
            async with session.get(url=url["url"], timeout=360) as response:
                resp = await response.read()
                title, body = self.get_text_selectolax(resp)
                result.update({"title": title, "body": body, "inaccessible": False})

        except Exception as e:
            self.html_stat["inaccessible"] += 1

        self.html_stat["total"] += 1
        if self.html_stat["total"] % 1000 == 0:
            self.log_info(f"{self.html_stat} : {datetime.datetime.now().time()}")
        return result

    async def bound_get_text(self, url, session):
        async with self.sem:
            return await self.get_text_from_url(url, session)

    async def create_new_dataset_from_urls(self, id, urls_list):
        cache_info = load_json(self.cache_path)
        if id in cache_info["saved_batches"]:
            return

        self.html_stat = {"inaccessible": 0, "total": 0}
        self.sem = asyncio.Semaphore(1000)
        session_timeout = aiohttp.ClientTimeout(total=None)
        async with aiohttp.ClientSession(timeout=session_timeout,
                                         connector=aiohttp.TCPConnector(limit=0, ssl=False)) as session:
            ret = await asyncio.gather(*[self.bound_get_text(url, session) for url in urls_list])

        out_file = f"{self.file_out_dir}/{id}.txt"
        log_file = f"{self.file_out_dir}/{id}_log.txt"

        create_file(out_file)
        save_json(out_file, ret)
        save_json(log_file, self.html_stat)

        if self.html_stat["inaccessible"] < self.html_stat["total"] / 2:
            cache_info["saved_batches"].append(id)
            save_json(self.cache_path, cache_info)

        self.log_info(f"batch id: {id} with stats {self.html_stat}")


    def run_download_html(self, left=0, right=sys.maxsize):
        if platform.system() == 'Windows':
            asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

        urls_list = load_json(self.urls_path)
        if not is_exist(self.cache_path) or load_json(self.cache_path)["num_batch"] != self.batch_size:
            self.create_cache_info()

        range_urls = int(len(urls_list) / self.batch_size)
        acc = left * range_urls
        for i in range(left, min(right, self.batch_size)):
            start_time = datetime.datetime.now().time()
            self.log_info(f"batch id: {i}, start time: {start_time}")
            asyncio.run(self.create_new_dataset_from_urls(i, urls_list[acc: acc + range_urls]))
            self.log_info(
                f"batch id: {i}, start time: {start_time}, end time: {datetime.datetime.now().time()}")

            acc += range_urls

    def merge_dataset_parts(self):
        self.merge_dataset_html_parts()
        self.merge_dataset_log_parts()

    def merge_dataset_html_parts(self):
        full_dataset = []
        self.log_info("start of html-dataset merging")
        for i in range(self.batch_size):
            try:
                file_html_text = load_json(f"{self.file_out_dir}/{i}.txt")
                full_dataset += file_html_text
            except Exception as e:
                pass

        self.log_info("end of html-dataset merging")
        save_json(f"new_dataset/msmarco-document.txt", full_dataset)

    def merge_dataset_log_parts(self):
        total_stat = {"inaccessible": 0, "total": 0}
        self.log_info("start of the stat dataset merging")
        for i in range(self.batch_size):
            try:
                sub_stat = load_json(f"{self.file_out_dir}/{i}_log.txt")
                total_stat["inaccessible"] += sub_stat["inaccessible"]
                total_stat["total"] += sub_stat["total"]
            except Exception as e:
                pass

        self.log_info("end of the stat dataset merging")
        save_json(f"new_dataset/msmarco-document_log.txt", total_stat)


    def create_cache_info(self):
        cache_info = {"num_batch" : self.batch_size, "saved_batches" : []}
        create_file(self.cache_path)
        save_json(self.cache_path, cache_info)

    def print_not_cached_ids(self):
        cache_info = load_json(self.cache_path)
        for i in range(self.batch_size):
            if i not in cache_info["saved_batches"]:
                print(i, end=" ")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--urls', type=str, help='path to the file containing the list of the dataset URLs')
    parser.add_argument('--dataset', type=str, help='filepath to the dataset')
    parser.add_argument('--batch', type=int, help='count of batch')
    parser.add_argument('--run', action='store_true', help="start html-pages download")
    parser.add_argument('--run_range', nargs=2, metavar=('l, r'), type=int,  help='launch on a segment of batches')
    parser.add_argument('--merge_html', action='store_true', help='merge part of the html dataset')
    parser.add_argument('--merge_log', action='store_true', help='merge part of the log dataset')
    parser.add_argument('--merge', action='store_true', help="merge all parts of the dataset")
    parser.add_argument('--not_cached', action='store_true', help="show list of not cached batch ids")

    args = parser.parse_args()
    adp = AsyncDatasetPreparer(args.dataset, args.urls, args.batch)
    if args.run:
        adp.run_download_html()
    elif args.run_range:
        l, r = args.run_range
        adp.run_download_html(l, r)

    if args.merge:
        adp.merge_dataset_parts()
    elif args.merge_html:
        adp.merge_dataset_html_parts()
    elif args.merge_log:
        adp.merge_dataset_log_parts()

    if args.not_cached:
        adp.print_not_cached_ids()
