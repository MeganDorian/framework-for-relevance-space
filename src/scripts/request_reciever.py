import argparse
from urllib.error import HTTPError
from googlesearch import search
from fake_useragent import UserAgent
import random
from utils import load_file


def save_urls(queries: list, out_filename: str, total_queries: int, country: str, lang: str):
    out_filename = "urls.txt" if out_filename is None else out_filename
    total_queries = 10 if total_queries is None else total_queries
    country = "us" if total_queries is None else country
    lang = "en" if lang is None else lang

    open(out_filename, "w").close()
    result, cnt_queries = dict(), 0
    ua = UserAgent().random
    for query in queries:
        result[query], is_searched = [], False
        print(f"{cnt_queries}: {query}")
        try:
            with open(out_filename, 'a+') as out_f:
                for url in search(query, stop=total_queries, country=country, lang=lang, user_agent=ua,
                                  pause=random.randint(50, 60)):
                    #proxy="165.227.81.188:9987"
                    is_searched = True
                    result[query].append(url)
                    out_f.write(str(url) + '\n')

            if not is_searched:
                print(f"{query} is skipped")
                ua = UserAgent().random
                queries.append(query)
            else:
                cnt_queries += 1

        except HTTPError as e:
            print(e.msg)

    print(f"total queries:  {cnt_queries}")
    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-q", "--queries", type=str)
    parser.add_argument("-o", "--out", type=str)
    parser.add_argument("-t", "--total", type=int)
    parser.add_argument("-c", "--country", type=str)
    parser.add_argument("-l", "--lang", type=str)

    args = parser.parse_args()

    if args.queries:
        save_urls(load_file(args.queries), args.out, args.total, args.country, args.lang)
